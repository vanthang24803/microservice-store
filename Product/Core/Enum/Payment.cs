namespace Product.Core.Enum
{
    public enum Payment
    {
        COD,
        BANK,
        SHOP,
    }
}
