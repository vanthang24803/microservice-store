namespace Product.Core.Enum
{
    public enum Status
    {
        PENDING,
        CREATE,
        SHIPPING,
        SUCCESS,
    }
}