namespace Product.Core.Utils
{
    public class QueryReview
    {
        public string? Status { get; set; } = null;

        public string? Star { get; set; }

    }
}