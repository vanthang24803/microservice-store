
namespace Product.Core.Utils
{
    public class QueryObjectOrder
    {
        public string? Status { get; set; } = null;

        public string? Month { get; set; } = null;

        public string? Time { get; set; } = null;
    }
}