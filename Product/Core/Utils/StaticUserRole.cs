namespace Product.Core.Utils
{
    public static class StaticUserRole
    {
        public const string MANAGER = "MANAGER";
        public const string ADMIN = "ADMIN";
        public const string USER = "USER";
    }
}