namespace Product.Core.Utils
{
    public class StaticRankRole
    {
        public static string Bronze = "Bronze";
        public static string Silver = "Silver";
        public static string Gold = "Gold";
        public static string Platinum = "Platinum";
        public static string Diamond = "Diamond";
    }
}