namespace Product.Core.Dtos.Response
{
    public interface IResponse
    {
        bool IsSucceed { get; set; }
    }
}