

namespace Product.Core.Dtos.Auth
{
    public class ResetPasswordDto
    {
         public string NewPassword { get; set; } = string.Empty;
    }
}